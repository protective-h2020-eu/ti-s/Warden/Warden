#!/usr/bin/env bash
#   Use this script to set up certs for warden

cd keys
rm *

# Create server cert.
openssl genrsa -des3 -out server.key 1024
openssl req -new -key server.key -out server.csr -subj '/CN=server.warden.ait.ie'
cp server.key server.key.org
openssl rsa -in server.key.org -out server.key
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

# Create ca cert.
openssl genrsa -des3 -out ca.key 4096
openssl req -new -x509 -days 365 -key ca.key -out ca.crt -subj '/CN=ca.warden.ait.ie'

# Create client cert.
openssl genrsa -des3 -out client.key 1024
openssl req -new -key client.key -out client.csr -subj '/CN=sender.warden.ait.ie'
cp client.key client.key.org
openssl rsa -in client.key.org -out client.key
openssl x509 -req -days 365 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

cd ..

# Alternative
# openssl req -x509 -sha256 -newkey rsa:2048 -keyout cert.key -out cert.pem -days 1024 -nodes -subj '/CN=www.fake-example.com'