#!/bin/bash

umask 0111

filer_dir="/var/spool/warden_sender"
src_ip=$1
failures=$2
detect_time=$(date --date="@$3" --rfc-3339=seconds)
create_time=$(date --rfc-3339=seconds)
node_name="org.example.fail2ban.ssh"

uuid() {
        for ((n=0; n<16; n++)); do  
                read -n1 c < /dev/urandom
                LC_CTYPE=C d=$(printf '%d' "'$c")
                s=''
                case $n in
                        6) ((d = d & 79 | 64));;   
                        8) ((d = d & 191 | 128));;  
                        3|5|9|7) s='-';; 
                esac
                printf '%02x%s' $d "$s"
        done
}

event_id=$(uuid)

cat >"$filer_dir/tmp/$event_id" <<EOF
{
   "Format": "IDEA0",
   "ID": "$event_id",
   "DetectTime": "$detect_time",
   "CreateTime": "$create_time",
   "Category": ["Attempt.Login"],
   "Description": "SSH dictionary/bruteforce attack",
   "ConnCount": $failures,
   "Note": "IP attempted $failures logins to SSH service",
   "Source": [{
      "IP4": ["$src_ip"],
      "Proto": ["tcp", "ssh"]
   }],
   "Target": [{
       "Type": ["Anonymised"],
       "IP4": ["192.0.2.0/24"],
       "Anonymised": true,
       "Proto": ["tcp", "ssh"],
       "Port": [22]
   }],
   "Node": [{
         "Name": "$node_name",
         "SW": ["Fail2Ban"],
         "Type": ["Log", "Statistical"]
   }]
}
EOF

mv "$filer_dir/tmp/$event_id" "$filer_dir/incoming"
