#!/bin/bash

umask 0111

filer_dir="/var/mentat/spool/_wardenout"
src_ip=$1
failures=$2
detect_time=$(date --date="@$3" --rfc-3339=seconds)
create_time=$(date --rfc-3339=seconds)
node_name="org.example.fail2ban.blacklist"

uuid() {
        for ((n=0; n<16; n++)); do  
                read -n1 c < /dev/urandom
                LC_CTYPE=C d=$(printf '%d' "'$c")
                s=''
                case $n in
                        6) ((d = d & 79 | 64));;   
                        8) ((d = d & 191 | 128));;  
                        3|5|9|7) s='-';; 
                esac
                printf '%02x%s' $d "$s"
        done
}

event_id=$(uuid)

cat >"$filer_dir/tmp/$event_id" <<EOF
{
   "Format" : "IDEA0",
   "ID" : "$event_id",
   "DetectTime" : "$detect_time",
   "CreateTime" : "$create_time",
   "Category" : ["Abusive.Spam"],
   "Description" : "Blacklisted host",
   "Note" : "Block duration: 3600. IP was blacklisted, is listed on more than 5 public blacklists",
   "Source" : [{
      "Type": ["Spam"],
      "IP4" : ["$src_ip"],
      "Proto": ["tcp", "smtp"]
   }],
   "Node" : [{
         "Name" : "$node_name",
         "SW" : ["Fail2Ban"],
         "Type" : ["Log", "Statistical"]
   }],
   "_CESNET" : {
      "Impact" : "IP was blacklisted, is listed on more than 5 public blacklists",
      "EventTemplate" : "f2b-001"
   }
}
EOF

mv "$filer_dir/tmp/$event_id" "$filer_dir/incoming"
