Dobrý den,

přišlo nám upozornění od cizí organizace, že Váš počítač nabízel produkty uvedené níže.
Do doby než nám zašlete vysvětlení Vám byla zablokována registrace.

Notice ID: {id}
Protocol: BitTorrent
IP Address: {ip} 
File Name: {filename}
Timestamp: {timestamp}

Chtěli bychom Vás požádat o prověření, zda nedochází k porušování autorských práv z této stanice.
Prosíme, ověřte stav Vaší stanice, a zašlete nám vysvětlující zprávu, kde uvedete kroky, které jste realizoval, aby dále k tomuto jevu nedocházelo.

Bližší informace o problematice naleznete na adrese:
http://idoc.vsb.cz/cit/tuonet/pravidla/az/

---

Hello,

we received an information from foreign organization, that your PC shared (uploaded) copyrighted material listed below.
Your registration (access to computer network and internet) will be suppressed until you send us an explanation.

Notice ID: {id}
Protocol: BitTorrent
IP Address: {ip} 
File Name: {filename}
Timestamp: {timestamp}

We would like to ask you for verify your PC (installed software), if there is some software which may be the cause for breaking the copyright act.
Please check your PC and send us your deliverance, including the steps you realized to avoid this in the future.
