#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os, sys
from netaddr import all_matching_cidrs # apt-get install python-netaddr

from warden_client import Client, Error, read_cfg

# pip install python-rtkit
from rtkit.resource import RTResource
from rtkit.authenticators import QueryStringAuthenticator
from rtkit.errors import RTResourceError
from rtkit import set_logging

import logging
set_logging('debug')
logger = logging.getLogger('rtkit')

def createTicket (config, body, ip):

        template = config.get('rtsubject') 
	# "rtsubject": "{category} {ip} {login} {other}"
	subject = template.format(category=config.get("category", "sdileni"), ip="("+ip+")", login="", other=config.get("other", " - zablokovano"))

	content = {
	    'content': {
			'Queue': config.get('rtqueue'),
			'Subject': subject, 
			'Text': body,
	    }
	}
	
	try:
	    resource = RTResource(config.get('rtrest'), config.get('rtuser'), config.get('rtpass'), QueryStringAuthenticator)
	    response = resource.post(path='ticket/new', payload=content,)

	    logger.info(response.parsed)

	except RTResourceError as e:
	    logger.error(e.response.status_int)
	    logger.error(e.response.status)
	    logger.error(e.response.parsed)


def main():
	config = read_cfg("warden_client-rt.cfg")

	# Allow inline or external Warden config
	wconfig = config.get("warden", "warden_client.cfg")

	if isinstance(wconfig, basestring):
		wconfig = read_cfg(wconfig)

	wclient = Client(**wconfig) 

	btconfig = config.get("bt", None)
	matching_cidrs = btconfig.get('matching_cidrs')

	with open(btconfig.get('template', None)) as f:
	       template = f.read()


	filt = {}
	conf_filt = config.get("filter", {})
	# Extract filter explicitly to be sure we have right param names for getEvents
	for s in ("cat", "nocat", "tag", "notag", "group", "nogroup"):
		filt[s] = conf_filt.get(s, None)

	ret = wclient.getEvents(**filt)
	
	for e in ret:
		try:
			ip = e.get("Source")[0].get("IP4")[0]
			id = e.get("ID")
			timestamp = e.get("DetectTime")
			filename = e['Attach'][0]['FileName'][0]
		except:
			pass	

		message = template.format(id=id, ip=ip, filename=filename, timestamp=timestamp)
		#print message

		if all_matching_cidrs(ip, btconfig.get('matching_cidrs', None)):
			createTicket(config.get('rt',None), message, ip)

if __name__ == "__main__":
    main()
