#!/bin/bash
#
### BEGIN INIT INFO
# Provides:          warden_filer_sender
# Required-Start:    $local_fs $syslog
# Required-Stop:     $local_fs $syslog
# Should-Start:      $network $named
# Should-Stop:       $network $named
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Warden Filer - sender
### END INIT INFO

DAEMON_NAME=warden_filer
FUNC=sender
DAEMON_PATH=/usr/local/bin/"$DAEMON_NAME".py
SERVICE_NAME="${DAEMON_NAME}_${FUNC}"
PID=/var/run/"$DAEMON_NAME"/"$FUNC".pid
CONFIG=/etc/"$DAEMON_NAME".cfg

# Try Debian & Fedora/RHEL/Suse sysconfig
for n in default sysconfig; do
	[ -f /etc/$n/"$SERVICE_NAME" ] && . /etc/$n/"$SERVICE_NAME"
done

# Fallback
function log_daemon_msg () { echo -n "$@"; }
function log_end_msg () { [ $1 -eq 0 ] && echo " OK" || echo " Failed"; }
function status_of_proc () { [ -f "$PID" ] && ps u -p $(<"$PID") || echo "$PID not found."; }

[ -f /lib/lsb/init-functions ] && . /lib/lsb/init-functions

ACTION="$1"

case "$ACTION" in
	start)
		mkdir -p "${PID%/*}"
		log_daemon_msg "Starting $SERVICE_NAME" "$SERVICE_NAME"
		start_daemon -p "$PID" "$DAEMON_PATH" -c "$CONFIG" --pid_file "$PID" --daemon "$FUNC"
		log_end_msg $?
		;;
	stop)
		log_daemon_msg "Stopping $SERVICE_NAME" "$SERVICE_NAME"
		killproc -p "$PID" "$DAEMON_PATH"
		log_end_msg $?
		;;
	restart|force-reload)
		$0 stop && sleep 2 && exec $0 start
		;;
	status)
		status_of_proc -p "$PID" "$DAEMON_PATH" "$SERVICE_NAME"
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 2
		;;
esac
