Warden Server, Warden Client, Warden Filer and Docker
=====================================================
This git branch contains modifications to Warden to support docker.
Warden Server has also been modified so that it doesn't need any certs. Certs are optional for the Warden Client and Warden Filer. **Not using certs is insecure and not suited to production.** In future we will introduce certificates for all Warden components.

Prerequisites
-------------
To start, make sure Git, Docker and Docker Compose are installed on your system.

- For the latest Windows 10, install Docker for Windows. This is the preferred way of working with Windows.
If you are using an earlier Windows then you must use Docker Toolbox (not recommended, upgrade Windows is the preferred option).

- For the Mac, install Docker for Mac (NOT Docker Toolbox). It should be easier to upgrade to the latest macOS, if necessary, to avoid using Docker Toolbox. 

- For other systems use the recommended approach for that system.

Install
-------
Clone this repository, and checkout the warden-3-docker branch.

`cd` into the `warden3/warden_client` folder.

Run the following in a terminal. 

```
docker build -t warden_client:3.0-beta2
```

Optional Steps (Not recommended initially)
--------------

`cd` into the `warden3/contrib/warden_filer` folder.

Modify `warden_filer_receiver.cfg` and `warden_filer_sender.cfg` only if neccessary.

`cd` into the `warden3/warden_server` folder.

Modify `warden_server.cfg` only if neccessary.

Required Steps
--------------

`cd` into the `warden3/warden_client` folder.

Copy the `ca.pem`, `key.pem` and `cert.pem` files used to identify the Warden Filer Receiver to the CESNET Warden Server into the `keys` folder. See further details in the Warden Client `README`. This will also explain how to arrange with CESNET access to their Warden Server.

`cd` into the `warden3/warden_server` folder.

Modify the `.env` file to set environment variables to configure the various components, including MySQL.

Run the following in a terminal. 

```
docker-compose up --build -d
```

Once this is done:

- Warden Filer Receiver is up (with certificates) and talking to CESNET Warden Server, see README on how to arrange with CESNET access to their Warden Server
- Protective Warden Server and Warden Filer Sender are up (without certificates) 

Data flows from CESNET Warden Server to PROTECTIVE Warden Server as follows:

- CESNET Warden Server
- PROTECTIVE Warden Filer Receiver
- shared data folder
- PROTECTIVE Warden Filer Sender
- PROTECTIVE Warden Server
- PROTECTIVE MySQL

Note that PROTECTIVE Warden Filer Sender is automatically registered with PROTECTIVE Warden Server, based on the contents of the `.env` file.
This is done in `warden_server/.whiskey/action_hooks/deploy`

More detailed information on how Warden is configured and used can be found in the respective **README** files in the Warden folders.